package com.voitech.devtest.whatdoido;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * What do I do class is typical example of lazy evaluation strategy which
 * delays evaluation of expression until its value is needed .We can avoid
 * performance decreasing by avoiding needless calculation of y(), we have
 * ability set different initialization strategies for Object o which are
 * interchangeable at Runtime not at compile time(In Object o constructor for
 * example). WhatDoIDo has interface in Constructor which will perform
 * initialization strategy and other calculations. That kind of implementation
 * introduce Strategy pattern which I mentioned at last interview. WhatDoIDo is
 * creating wrapped object and exposing it through package scoped method z(), it
 * also use a Boolean to store info about object creation, and that Boolean
 * inform as that Object should be created only once, that’s why method is
 * blocked by synchronized so only one Thread can access it at time to assure
 * that Object will be created only once.
 *
 * As you can see on my refactored code instead of synchronizing whole function
 * getWrappedObject() I used AtomicBoolean which is set to false at beginning.
 * It is atomically updated flag, provide atomic operations which are performed
 * as single unit of work without the possibility of interference from other
 * operations and Non-blocking Compare-And-Swap implementation.
 *
 * For Testing I used TestNG framework and netBeans profiler. Test was invoked
 * 600 hundred times an each Thread is calling 100 000 times method
 * getWrappedObject() or y(). After refactoring class pass test 5,90855106888361
 * times faster than with blocking whole block. Please see attachment for
 * details: - LazyEvaluationNGTest.png - WhatDoIDoNGTest.png
 *
 * @author Wojciech Koszycki
 * @param <T>
 */
public class LazyEvaluation<T> {

    private final InitializationStrategy<T> creationStrategy;
    private final AtomicBoolean objectInitialized = new AtomicBoolean(false);
    private T o;

    public LazyEvaluation(InitializationStrategy<T> concreteStrategy) {
        this.creationStrategy = concreteStrategy;
    }

    T getWrappedObject() {

        if (objectInitialized.compareAndSet(false, true)) {
            o = creationStrategy.createWithStrategy();
        }
        return o;
    }

    public interface InitializationStrategy<T> {

        T createWithStrategy();
    }
}
