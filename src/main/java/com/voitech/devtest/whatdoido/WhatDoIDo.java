package com.voitech.devtest.whatdoido;


public class WhatDoIDo {

    private X x;
    private boolean b;
    private Object o;

    public WhatDoIDo(X x) {
        this.x = x;
    }

    synchronized Object getWrappedObject() {

        if (!b) {
            o = x.y();
            b = true;
        }

        return o;
    }

    public interface X {
        Object y();
    }
}