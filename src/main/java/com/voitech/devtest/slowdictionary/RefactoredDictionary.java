package com.voitech.devtest.slowdictionary;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Serious flaw of Slow Dictionary is being non thread safe (method
 * getAllWords() is returning Set which can be modified), secondly blocking all
 * methods by synchronizing them will cause poor performance and Memory issue.
 * Anyway if in this particular case performance is critical instead of throwing
 * exceptions functions should return something to notify if operation succeed.
 * Throwing exceptions is ‘expensive’ and exception is exceptional situation not
 * getting translated word or adding to dictionary word that exist going to be
 * maybe 30% of all cases so it’s becoming common special treatment situation.
 * Firstably I get rid of all synchronized keywords .At this particular case
 * writing is critical for concurrency (overwriting word that exist) reading is
 * not so. Here comes ConcurrentHashMap which is not blocking reading almost at
 * all (except minority cases) and for concurrent writers to add and update
 * values while only acquiring locks over localized segments of the internal
 * data structure. Besides that it has enormous performance which is one of the
 * SlowDictionary problem. ConcurrentHashMap has 3 parameters, initialCapacity,
 * loadFactor and concurrencyLevel and this last one can cause problems(default
 * value is 16) and the number of instances allocated can grow dramatically.
 * Depending upon usage of ConcurrentHashMap, ConcurrentHashMap can end up
 * adding serious memory and GC load to system. Instead of throwing exceptions
 * am returning result (could be Boolean). For getAllWords method I return
 * unmodifiable set. Test showed that adding to dictionary would be
 * 11,64797136038186 times faster than before and translating and
 * 8,408348457350272 times faster for translating. Also heap size is more
 * constant and at maximum point reach 280MB and for SlowDictionary up to 350MB.
 * For more details please see attachments : - RefacotredDictionaryNGTest.png -
 * SlowDictionaryNGTest.png
 *
 * @author wojtek
 */
public class RefactoredDictionary {

    private final ConcurrentHashMap<String, String> dict;

    public RefactoredDictionary() {
        this.dict = new ConcurrentHashMap<>(8, 0.9f, 1);
    }

    public String translate(String word) {
        String result = dict.get(word);
        if (result == null) {
            return word + " not found";
        }
        return result;
    }

    public String addToDictionary(String word, String translation) {
        if (dict.containsKey(word)) {
            return word + " already exists.";
        }
        dict.put(word, translation);
        return word + "added";
    }

    public Set<String> getAllWords() {
        return Collections.unmodifiableSet(dict.keySet());
    }
}
