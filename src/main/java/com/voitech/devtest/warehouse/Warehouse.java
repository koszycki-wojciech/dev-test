package com.voitech.devtest.warehouse;

/**
 * ConcreteWarehouse is singleton and implements Warehouse interface, is
 * managing registering Objects that implements Consumable and Supplyable
 * interfaces .There are Product types which are Enumereted. ConcreteSupplier
 * doesn’t implement Consumable interface but it’s holding reference to it and
 * can change the implementation on runtime same ConcreteSupplier.
 *
 * @author Wojciech Koszycki
 */
public interface Warehouse {

    ProductType buy(ProductType productType);

    ProductType sale(ProductType productType);

    void registerSupplier(Supplyable supplier);

    void registerConsumer(Consumable consumer);
}
