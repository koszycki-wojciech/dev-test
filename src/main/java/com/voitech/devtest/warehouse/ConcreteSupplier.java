package com.voitech.devtest.warehouse;

/**
 *
 * @author wojtek
 */
public class ConcreteSupplier {

    private Supplyable supplyable;

    public ConcreteSupplier(Supplyable supplyable) {
        this.supplyable = supplyable;
    }

    public Supplyable getSupplyable() {
        return supplyable;
    }

    public void setSupplyable(Supplyable supplyable) {
        this.supplyable = supplyable;
    }

}
