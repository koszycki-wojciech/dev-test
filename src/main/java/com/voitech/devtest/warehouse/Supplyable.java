package com.voitech.devtest.warehouse;

public interface Supplyable {

	void sale();
	
	ProductType getProductType();
}
