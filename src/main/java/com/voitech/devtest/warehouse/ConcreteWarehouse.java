/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.devtest.warehouse;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author wojtek
 */
public class ConcreteWarehouse implements Warehouse{

    private static ConcreteWarehouse instance= new ConcreteWarehouse();
    private Map<ProductType,Supplyable> suppliers= new EnumMap<ProductType, Supplyable>(ProductType.class);
    private Map<ProductType,Consumable> consumers= new EnumMap<ProductType, Consumable>(ProductType.class);
    
    private ConcreteWarehouse() {
    }

    public static ConcreteWarehouse getInstance() {
        return instance;
    }

    
    
    @Override
    public void registerSupplier(Supplyable supplier) {
        suppliers.put(supplier.getProductType(), supplier);
    }

    @Override
    public void registerConsumer(Consumable consumer) {
        consumers.put(consumer.getProductType(), consumer);
    }

    @Override
    public ProductType buy(ProductType productType) {
        if (consumers.containsKey(productType)) {
            consumers.remove(productType);
            return productType;
        }
        return null;
    }

    @Override
    public ProductType sale(ProductType productType) {
        if (suppliers.containsKey(productType)) {
            suppliers.remove(productType);
            return productType;
        }
        return null;
    }
    
    
    
}
