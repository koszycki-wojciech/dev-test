/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voitech.devtest.warehouse;

/**
 *
 * @author wojtek
 */
public class ConcreteConsumer  {
    private Consumable consumable;

    
    
    public ConcreteConsumer(Consumable consumable) {
        this.consumable = consumable;
    }
    
    public Consumable getConsumable() {
        return consumable;
    }

    public void setConsumable(Consumable consumable) {
        this.consumable = consumable;
    }
    
}
