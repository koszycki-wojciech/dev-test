package com.voitech.devtest.whatdoido;

import com.voitech.devtest.whatdoido.WhatDoIDo;
import org.testng.annotations.Test;
/**
 *
 * @author wojtek
 */
public class WhatDoIDoNGTest {
    
    private WhatDoIDo instance;

  
    @Test(threadPoolSize=1000,invocationCount=600)
    public void testGetWrappedObject() {
        instance = new WhatDoIDo(new WhatDoIDo.X() {
            @Override
            public Object y() {
                return new Object();
            }
        });
        for (int i = 0; i < 100000; i++) {
            instance.getWrappedObject();
        }
    }
}
