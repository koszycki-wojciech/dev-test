package com.voitech.devtest.whatdoido;

import com.voitech.devtest.whatdoido.LazyEvaluation;
import org.testng.annotations.Test;

/**
 *
 * @author wojtek
 */
public class LazyEvaluationNGTest {
    private LazyEvaluation instance;
    

    @Test(threadPoolSize=1000,invocationCount=600)
    public void testGetWrappedObject() {
        instance = new LazyEvaluation(new LazyEvaluation.InitializationStrategy() {
            @Override
            public Object createWithStrategy() {
                return new Object();
            }
        });
        for (int i = 0; i < 100000; i++) {
            instance.getWrappedObject();
        }
        
    }
}