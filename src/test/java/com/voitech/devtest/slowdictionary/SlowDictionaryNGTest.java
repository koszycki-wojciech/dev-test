package com.voitech.devtest.slowdictionary;

import java.util.Random;
import java.util.Set;
import org.testng.annotations.Test;

/**
 *
 * @author wojtek
 */
public class SlowDictionaryNGTest {

    private final Random random = new Random();
    private final SlowDictionary instance = new SlowDictionary();

    /**
     * Test of addToDictionary method, of class SlowDictionary.
     */
    @Test(threadPoolSize = 1000, invocationCount = 500)
    public void testAddToDictionary() {
        for (int i = 0; i < 10000; i++) {
            try {
                instance.addToDictionary(String.valueOf(random.nextInt(80000)), "translation");
            } catch (IllegalArgumentException e) {
            }

        }
        Set result = instance.getAllWords();

        System.out.println("Slow result.size()= " + result.size());
    }

    @Test(threadPoolSize = 1000, invocationCount = 500)
    public void testTranslate() {

        for (int i = 0; i < 10000; i++) {
            try {
                instance.translate(String.valueOf(random.nextInt(80000)));
            } catch (IllegalArgumentException e) {
            }

        }
    }
}
